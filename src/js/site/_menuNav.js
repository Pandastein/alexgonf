window.site = (window.site || {});

/**
 * Init Mobile Menu related scripts
 * @class Site
 * @static
 */
site.MenuNav = (function MenuNav() {

  'use strict';

  /**
   * jQuery elements
   * @private
   */


  /**
   * Has the class been initialized?
   * @private
   */
  var inited = false;

  /**
   * Initializes the class.
   * @public
   */
  var init = function () {

    // Abort if already initialized
    if (inited) {
      return false;
    }

    inited = true;

    var mobileBurger = document.querySelector('.mobile-burger');

    mobileBurger.addEventListener('click', showNavbar);

    return true;

  };

  var showNavbar = function(event) {
    var target = event.target || event.srcElement;
    var $mobileNavbar = document.querySelector('.navbar');


    console.log(target);
    target.classList.toggle('icon-menu');
    target.classList.toggle('icon-cancel');
    
    $mobileNavbar.classList.toggle('is-open');
  };

  // Expose public methods & properties
  return {
    init: init,
  };

}());
