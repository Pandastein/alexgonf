/* -- A listener to ensure the fonts we need to use have been loaded */

if (document.documentElement.className.indexOf("fonts-loaded") < 0) {
    var fontello = new FontFaceObserver("fontello", {});

    Promise.all([fontello.load('')]).then(function () {

        document.documentElement.className += " fonts-loaded";
    });
}