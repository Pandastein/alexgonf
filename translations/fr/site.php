<?php // This array will not be prefixed with 'fr.*'

return [

  'All Rights Reserved' => 'Tous Droits Réservés',
  'Education' => 'Études',
  'Contact' => 'Contacte',
  'Follow me on' => 'Me suivre sur',
  'Developed by Franckj.ca' => 'Développé par Franckj.ca'

];
